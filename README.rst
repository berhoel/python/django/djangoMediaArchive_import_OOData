==================================
 djangoMediaArchive_import_OOData
==================================

Addon for `djangoMediaArchive
<https://github.com/berhoel/djangoMediaArchive>`_. Import data for
djangoMediaArchive from LibreOffice spreadsheet.

Quick start
===========

1. Add ``djangoMediaArchive_import_OOData`` to your ``INSTALLED_APPS`` setting like this::

    INSTALLED_APPS = [
        ...
        'berhoel.django.media_ooimport',
    ]

2. Run ``poetry run python manage.py migrate`` to create the djangoMediaArchive_import_OOData models.

3. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a inspect media data (you'll need the Admin app enabled).

4. Run ``poetry run media_ooimport ~/Dokumente/Data.ods`` to import the media information.
