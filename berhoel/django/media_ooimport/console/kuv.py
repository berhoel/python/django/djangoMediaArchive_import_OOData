"""Process the `k&v` sheet."""

from __future__ import annotations

from typing import TYPE_CHECKING, NamedTuple

from berhoel.django.media.models import DVD, Person, Purchase, Sell
from berhoel.django.media_ooimport.models import SaveLine
from berhoel.helper import count_with_msg, timed_process_msg_context

if TYPE_CHECKING:
    from berhoel.odf.ods import Cell, Table

__date__ = "2024/08/09 23:44:30 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# line in "k&v" sheet where header line is located.
K_V_HEADER = 6


class KaufenUndVerkaufenRow(NamedTuple):
    """Function input."""

    title: Cell | None
    kaufdatum: Cell | None
    kaufpreis: Cell | None
    verkauft_am: Cell | None
    verkaufpreis: Cell | None
    familie: Cell | None
    verkaeufer: Cell | None
    kaeufer: Cell | None


MAX_ROW_LENGTH = 8


class KaufenUndVerkaufen:
    """Process the `k&v` sheet."""

    def __init__(self, table: Table) -> None:
        """Initialize class instance.

        Args:
        table (Table): OpenOffice table.
        """
        self.table = table

    def process(self) -> None:  # noqa:C901,PLR0912
        """Read sheet 'k&v' (kaufen und verkaufen)."""
        purchases: list[tuple[Purchase, list[DVD]]] = []
        line_ids = []
        rows = iter(self.table.rows)
        _ = list(zip(range(K_V_HEADER), rows))
        for row_, line in zip(
            rows, count_with_msg("Importing k&v, line", K_V_HEADER + 1)
        ):
            row = (*row_.cells[:5], *row_.cells[7:8], *row_.cells[9:])[:MAX_ROW_LENGTH]
            data = KaufenUndVerkaufenRow(
                row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]
            )

            if data.title is None:
                raise ValueError
            if data.title.text is None:
                break
            if data.kaufdatum is not None:
                price = 0.0 if data.kaufpreis is None else data.kaufpreis.float
                if data.kaufdatum is None:
                    raise ValueError
                if data.kaufdatum.date is None:
                    raise ValueError
                purchase = Purchase(date=data.kaufdatum.date, price=price)
                if data.verkaeufer is None:
                    raise ValueError
                if data.verkaeufer.text is not None:
                    vendor, _ = Person.objects.get_or_create(name=data.verkaeufer.text)
                    purchase.vendor = vendor
                purchases.append((purchase, []))

            if data.familie is None:
                raise ValueError
            dvd = DVD.objects.create(
                name=data.title.text,
                adver_name=data.title.text,
                family=data.familie.text == "X",
            )
            line_ids.append(SaveLine(dvd=dvd, line=line))

            purchases[-1][1].append(dvd)

            if data.verkauft_am is not None and data.verkauft_am.date is not None:
                buyer = None
                if data.kaeufer is None:
                    raise ValueError
                if data.kaeufer.text is not None:
                    buyer, _ = Person.objects.get_or_create(name=data.kaeufer.text)
                if data.verkaufpreis is None:
                    raise ValueError
                sold, _ = Sell.objects.get_or_create(
                    date=data.verkauft_am.date,
                    price=data.verkaufpreis.float,
                    buyer=buyer,
                )
                sold.dvds.add(dvd)
                sold.save()

        with timed_process_msg_context("Store Purchase"):
            Purchase.objects.bulk_create(purchase for purchase, _ in purchases)
            for (purchase, dvds), _ in zip(
                purchases, count_with_msg("Store Purchase, line")
            ):
                purchase.media.set(dvds)
            purchase.save()
        with timed_process_msg_context("Store SaveLine"):
            SaveLine.objects.bulk_create(line_ids)  # type:ignore[attr-defined]
