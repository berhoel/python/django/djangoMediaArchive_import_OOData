"""Process the `Film` sheet."""

from __future__ import annotations

from typing import TYPE_CHECKING, NamedTuple

from berhoel.django.media.models import Film, Media, Person, Purchase, Seen
from berhoel.helper import count_with_msg

from ._helper import get_media

if TYPE_CHECKING:
    from berhoel.odf.ods import Cell, Table

__date__ = "2024/08/10 01:16:46 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


class FilmRow(NamedTuple):
    """Call Parameter."""

    date: Cell | None
    name: Cell | None
    index_: Cell | None
    options: Cell | None
    desc: Cell | None
    price: Cell | None


class Filme:
    """Convert information on films seen."""

    def __init__(self, table: Table) -> None:
        """Initialize class instance.

        Args:
          table (berhoel.odf.ods.Ods): OpenOffice table.
        """
        self.table = table

    def process(self) -> None:
        """Process film entries."""
        rows = zip(self.table.rows, count_with_msg("Importing 'filme', line"))
        _ = next(rows)
        for row_, _ in rows:
            row = FilmRow(*row_.cells[:6])
            if row.date is None:
                raise TypeError
            if row.date.date is None:
                break

            if (
                row.price is None
                or row.index_ is None
                or row.name is None
                or row.name.text is None
            ):
                raise TypeError
            media, index_ = get_media(
                media_in=row.price, index_=row.index_.text, name=row.name.text
            )
            if row.name is None or row.options is None or row.desc is None:
                raise TypeError
            film, _ = Film.objects.get_or_create(
                title=row.name.text,
                imdb_url="" if row.name.url is None else row.name.url,
                dvd_index=index_,
                options="" if row.options.text is None else row.options.text,
                medium=media,
                desc="" if row.desc.text is None else row.desc.text,
            )
            Seen.objects.create(date=row.date.date, watch_item=film)

            if row.price.float is None:
                raise TypeError
            if row.price.link is None and row.price.float > 0.0:
                vendor, _ = Person.objects.get_or_create(name=row.index_.text)
                purchase = Purchase.objects.create(
                    date=row.date.date, price=row.price.float, vendor=vendor
                )
                if not isinstance(media, Media):
                    raise TypeError
                purchase.media.set([media])
                purchase.save()
