"""Helper functionalities for media_ooimport."""

from __future__ import annotations

from typing import TYPE_CHECKING

from django.db.models import QuerySet

from berhoel.django.media.models import Broadcast, RentalVideo, Streaming, TheatreVisit
from berhoel.django.media_ooimport.models import (
    DVD,
    NameCinemaMap,
    NameMediaMap,
    NameRentalMap,
    NameStreamingServiceMap,
    SaveLine,
)

if TYPE_CHECKING:
    from django.db import models

    from berhoel.odf.ods import Cell


__date__ = "2024/08/10 01:43:35 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


def get_media(  # noqa:C901,PLR0912
    media_in: Cell,
    index_: str | None,
    name: str,
    season: int | None = None,
    subseason: int | None = None,
) -> tuple[models.Model, int | None]:
    """Determine media information."""
    r_index = None
    media: DVD | Broadcast | TheatreVisit | RentalVideo | Streaming
    if media_in.link is not None:
        line = media_in.link.get("line")
        if not isinstance(line, str):
            raise ValueError
        media = SaveLine.objects.get(line=int(line)).dvd  # type:ignore[attr-defined]
        if not isinstance(media, DVD):
            raise ValueError
        media.name = name
        media.season = season
        media.sub_season = subseason
        try:
            r_index = None if index_ is None else int(index_)
        except ValueError:
            r_index = None
    elif station := NameMediaMap.objects.filter(name=index_):  # type:ignore[attr-defined]
        if not isinstance(station, QuerySet):
            raise ValueError
        if len(station) != 1:
            msg = f"{index_=} {media_in=}"
            raise ValueError(msg)
        media = Broadcast(
            broadcast_service=station[0].media.broadcast_service,
            name=name,
            season=season,
            sub_season=subseason,
        )
    elif cinema := NameCinemaMap.objects.filter(name=index_):  # type:ignore[attr-defined]
        if not isinstance(station, QuerySet):
            raise ValueError
        if len(cinema) != 1:
            msg = f"{index_=} {cinema=}"
            raise ValueError(msg)
        media = TheatreVisit.objects.get_or_create(
            theatre=cinema[0].person,
            price=media_in.float,
            name=name,
        )[0]
    elif rental := NameRentalMap.objects.filter(name=index_):  # type:ignore[attr-defined]
        if not isinstance(station, QuerySet):
            raise ValueError
        if len(rental) != 1:
            msg = f"{index_=} {rental=}"
            raise ValueError(msg)
        media = RentalVideo.objects.get_or_create(
            company=rental[0].person,
            price=media_in.float,
            name=name,
            season=season,
            sub_season=subseason,
        )[0]
    elif service := NameStreamingServiceMap.objects.filter(name=index_):  # type:ignore[attr-defined]
        if not isinstance(station, QuerySet):
            raise ValueError
        if len(service) != 1:
            msg = f"{index_=} {service=}"
            raise ValueError(msg)
        media = Streaming.objects.get_or_create(
            streaming_service=service[0].person,
            name=name,
            season=season,
            sub_season=subseason,
        )[0]
    else:
        msg = f"{name=} {index_=} {media_in.text=}"
        raise ValueError(msg)
    media.save()
    return media, r_index
