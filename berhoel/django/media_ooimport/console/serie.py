"""Convert for Series."""

from __future__ import annotations

from typing import TYPE_CHECKING, NamedTuple

from berhoel.django.media.models import (
    Media,
    Person,
    Purchase,
    Season,
    Seen,
    Series,
    SeriesEpisode,
)
from berhoel.helper import count_with_msg
from berhoel.odf.ods import Cell

from ._helper import get_media

if TYPE_CHECKING:
    from berhoel.odf.ods import Table

__date__ = "2024/08/10 01:46:14 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


class SerieRow(NamedTuple):
    """Calling arguments."""

    date: Cell | None
    season: Cell | None
    episode: Cell | None
    disk: Cell | None
    index_: Cell | None
    diskset: Cell | None
    info: Cell | None
    desc: Cell | None
    dvd_link: Cell | None


MAX_ROW_LENGTH = 9


class Serie:
    """Represent series on import."""

    index_ = None

    def __init__(self, table: Table) -> None:
        """Initialize class instance."""
        self.table = table
        self.name = table.name
        self.is_active = not table.hidden
        if Serie.index_ is None:
            set_ = Series.objects.filter().order_by("-order_no")
            Serie.index_ = 1 if not set_ else set_[0].order_no
        Serie.index_ += 1

    def process(self) -> None:  # noqa:C901,PLR0912,PLR0915
        """Process Series information."""
        rows = zip(self.table.rows, count_with_msg(f"Importing '{self.name}', line"))

        first_row, _ = next(rows)
        title, *_ = first_row.cells
        if not isinstance(title, Cell):
            raise TypeError
        try:
            name = title.text
            url = "" if title.url is None else title.url
        except IndexError:
            if self.name not in {"Spejbl & Hurvinek", "Lucky Luke"}:
                raise

        if name is None or Serie.index_ is None:
            raise ValueError
        series = Series.objects.create(title=name, imdb_url=url, order_no=Serie.index_)

        # skip one row
        _ = next(rows)
        media = None
        for row_, _ in rows:
            row = (*row_.cells[:8], *row_.cells[9:])[:MAX_ROW_LENGTH]
            if len(row) < MAX_ROW_LENGTH:
                break
            data = SerieRow(*row)
            # Season
            if data.season is None:
                raise TypeError
            season = data.season.int
            if season is None:
                continue
            if data.diskset is None:
                raise TypeError
            diskset = data.diskset.float
            if (diskset is not None) and int(diskset) != diskset:
                sub_season = round((diskset - int(diskset)) * 10)
            else:
                sub_season = None
            # Media
            if data.dvd_link is not None and data.dvd_link.text is not None:
                if data.disk is None:
                    raise TypeError
                media, _ = get_media(
                    media_in=data.dvd_link,
                    index_=data.disk.text,
                    name=name,  # data.info.text,
                    season=season,
                    subseason=sub_season,
                )
            if media is None or season is None:
                continue
            season_, _ = Season.objects.get_or_create(series=series, season=season)
            # Episode
            if data.episode is None:
                raise TypeError
            episode: float | None = data.episode.float
            if episode is None:
                raise TypeError
            if int(episode) == episode:
                sub_episode = None
                episode = int(episode)
            else:
                sub_episode = round((episode - int(episode)) * 10)
                episode = int(episode)
            if data.info is None:
                raise TypeError
            episode_name = data.info.text
            imdb_url = "" if data.info.url is None else data.info.url
            index_ = None if data.index_ is None else data.index_.int
            desc = "" if data.desc is None else data.desc.text
            desc = "" if desc is None else desc
            series_episode, _ = SeriesEpisode.objects.get_or_create(
                title=episode_name,
                medium=media,
                imdb_url=imdb_url,
                dvd_index=index_,
                season=season_,
                episode=episode,
                sub_episode=sub_episode,
                desc=desc,
            )
            # Seen
            if data.date is None:
                raise TypeError
            date = data.date.date
            if date is not None:
                Seen.objects.create(date=date, watch_item=series_episode)

            if data.dvd_link is None:
                raise TypeError
            if (
                data.dvd_link.text is not None
                and data.dvd_link.link is None
                and data.dvd_link.float is not None
                and data.dvd_link.float > 0.0
            ):
                if data.season is None or data.date is None:
                    raise TypeError
                vendor, _ = Person.objects.get_or_create(name=data.season.text)
                if data.date.date is None:
                    raise TypeError
                purchase = Purchase.objects.create(
                    date=data.date.date, price=data.dvd_link.float, vendor=vendor
                )
                if not isinstance(media, Media):
                    raise TypeError
                purchase.media.set([media])
                purchase.save()
