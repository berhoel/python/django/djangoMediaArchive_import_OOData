"""Load Data into Django model."""

from __future__ import annotations

import argparse
from functools import cached_property
import os
from pathlib import Path
import sys
import traceback

import django
import pdbp  # type:ignore[import-untyped]
from rich.console import Console

from berhoel import helper
from berhoel.odf.ods import Ods

__date__ = "2024/08/10 01:32:28 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2015,2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# Full path to your django project directory
DJANGOPROJECT_HOME = Path(__file__).resolve().parents[4]
sys.path.append(f"{DJANGOPROJECT_HOME}")
if os.environ.get("DJANGO_SETTINGS_MODULE") is None:
    os.environ["DJANGO_SETTINGS_MODULE"] = "berhoel.django.media_ooimport.settings"

django.setup()

# These modules can only be included after django setup.
from berhoel.django.media_ooimport.models import (  # isort:skip # noqa: E402
    NameMediaMap,
    NameCinemaMap,
    NameRentalMap,
    ConvertedSheet,
    NameStreamingServiceMap,
)
from .kuv import KaufenUndVerkaufen  # isort:skip # noqa: E402
from .filme import Filme  # isort:skip # noqa: E402
from .serie import Serie  # isort:skip # noqa: E402
from berhoel.django.media.models import Broadcast, Person  # isort:skip # noqa: E402


CONSOLE = Console()

# Aliases for broadcasting services.
BCS = {
    "3SAT": ("3SAT", "3sat"),
    "7MAXX": ("7MAXX",),
    "ARD": ("ARD", "ARD Mediathek"),
    "Arte": ("Arte", "ARTE", "Arte Mediathek", "ARTE Mediathek"),
    "BR": ("BR",),
    "HR": ("HR",),
    "Kabel 1": ("Kabel 1", "kabeleins"),
    "NDR": ("NDR",),
    "one": ("one",),
    "PRO 7": ("PRO 7", "Pro7", "PRO7"),
    "RTL": ("RTL",),
    "RTL II": (
        "RTL II",
        "Rtl II",
    ),
    "RTL NITRO": ("RTL NITRO",),
    "SAT 1": (
        "SAT 1",
        "Sat1",
        "SAT1",
    ),
    "SIXX": ("SIXX",),
    "Super RTL": ("Super RTL",),
    "TELE5": ("TELE5",),
    "VOX": (
        "VOX",
        "VOX Now",
        "Vox",
    ),
    "ZDF": ("ZDF",),
    "ZDF Mediathek": ("ZDF Mediathek",),
    "ZDFneo": (
        "ZDFneo",
        "ZDF Neo",
        "zdf neo",
        "ZDF neo",
        "ZDFNeo",
    ),
    "mdr": ("mdr", "MDR"),
    "nitro": (
        "nitro",
        "NITRO",
    ),
}

# Aliases for cinemas.
CINEMAS = {
    "Abaton Kino": ("Abaton Kino",),
    "CinemaxX Hamburg-Dammtor": ("CinemaxX Hamburg-Dammtor",),
    "CinemaxX Hamburg-Wandsbek": ("CinemaxX Hamburg-Wandsbek",),
    "CineStar KristallPalast Bremen": ("CineStar KristallPalast Bremen",),
    "Cineworld Lünen": ("Cineworld Lünen",),
    "Holi": ("Holi", "HOLI"),
    "Passage Kino": ("Passage Kino",),
    "Planetarium Hamburg": ("Planetarium HH",),
    "Studio Bernsdoffstr.": ("Studio Bernsdoffstr.",),
    "UCI Mundsburg": ("UCI Mundsburg",),
    "UCI Othmarschen": ("UCI Othmarschen",),
    "UCI Smart City": ("UCI Smart City",),
    "Zeise Kino": ("Zeise Kino",),
    "alabama kino": ("alabama kino",),
    "Schlosstheater Münster": ("Schlosstheater Münster",),
}

# Aliases for video rental provider.
VIDEO_RENTAL: dict[str, tuple[str, ...]] = {"Bücherhalle": ("Bücherhalle",)}

# Aliases for streaming services.
STREAMING_SERVICES = {
    "Disney+": ("Disney+",),
    "NETFLIX": (
        "NETFLIX",
        "Netflix",
        "netflix",
    ),
    "Star Trek New Voyages": ("Star Trek New Voyages",),
    "YouTube": ("YouTube",),
    "amazon prime": (
        "Amazon Prime",
        "amazon Prime",
        "Amazon prime",
        "Amazon PRIME",
    ),
    "Joyn": ("Joyn",),
}


class SeriesOO:
    """Import data from LibreOffice spreadsheet."""

    def __init__(self, fname: str) -> None:
        self.fname = fname

        self.fill_media()

    @cached_property
    def ods(self) -> Ods:
        """Provide ods instance."""
        with helper.timed_process_msg_context(f"opening {self.fname}"):
            return Ods(self.fname)

    def process(self) -> None:
        """Process the importing."""
        tables = self.ods.tables

        for table in tables:
            name = table.name

            if ConvertedSheet.objects.filter(saved=name).exists():  # type:ignore[attr-defined]
                continue

            processor: KaufenUndVerkaufen | Filme | Serie
            with helper.timed_process_msg_context(f"Importing '{name}'"):
                if name == "k&v":
                    processor = KaufenUndVerkaufen(table)
                elif name == "Film":
                    processor = Filme(table)
                else:
                    processor = Serie(table)

                processor.process()

                ConvertedSheet.objects.create(saved=name)  # type:ignore[attr-defined]

    @staticmethod
    def fill_bcs() -> None:
        """Fill information on broadcast services media."""
        for name, aliases in BCS.items():
            pers, _ = Person.objects.get_or_create(name=name)  # type:ignore[attr-defined]
            bcs, _ = Broadcast.objects.get_or_create(name=name, broadcast_service=pers)
            for alias in aliases:
                NameMediaMap.objects.get_or_create(name=alias, media=bcs)  # type:ignore[attr-defined]

    @staticmethod
    def fill_person_maps(
        values: dict[str, tuple[str, ...]],
        table: type[NameCinemaMap | NameRentalMap | NameStreamingServiceMap],
    ) -> None:
        """Fill information in cinemas."""
        for name, aliases in values.items():
            pers = Person.objects.get_or_create(name=name)[0]
            for alias in aliases:
                table.objects.get_or_create(name=alias, person=pers)  # type:ignore[union-attr]

    def fill_media(self) -> None:
        """Prepare information in media."""
        with helper.timed_process_msg_context("Preparing Broadcast Services"):
            self.fill_bcs()
        with helper.timed_process_msg_context("Preparing Cinemas"):
            self.fill_person_maps(CINEMAS, NameCinemaMap)
        with helper.timed_process_msg_context("Preparing Rental Companies"):
            self.fill_person_maps(VIDEO_RENTAL, NameRentalMap)
        with helper.timed_process_msg_context("Preparing Streaming Service Companies"):
            self.fill_person_maps(STREAMING_SERVICES, NameStreamingServiceMap)


PARSER = argparse.ArgumentParser(
    description="Import data from LibreOffice spreadsheet into django media."
)
PARSER.add_argument("fname", type=Path, help="input ods file")


def main() -> None:
    """Execute the conversion."""
    args = PARSER.parse_args()

    try:
        sheet_ignore = {"suche", "Übersicht"}
        for sheet in sheet_ignore:
            ConvertedSheet.objects.get_or_create(saved=sheet)  # type:ignore[attr-defined]

        series_oo = SeriesOO(args.fname)

        with helper.timed_process_msg_context(f"Processing {args.fname}"):
            series_oo.process()
    except Exception:  # noqa:BLE001
        CONSOLE.print()
        _, _, tb = sys.exc_info()
        traceback.print_exc()
        pdbp.post_mortem(tb)


if __name__ == "__main__":
    main()
