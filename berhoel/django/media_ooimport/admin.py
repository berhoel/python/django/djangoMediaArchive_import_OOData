"""Django admin for series."""

from django.contrib import admin

from .models import (
    ConvertedSheet,
    NameCinemaMap,
    NameMediaMap,
    NameRentalMap,
    NameStreamingServiceMap,
    SaveLine,
)

__date__ = "2024/08/09 19:42:28 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2015 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


# Register your models here.

admin.site.register(SaveLine)
admin.site.register(ConvertedSheet)
admin.site.register(NameMediaMap)
admin.site.register(NameCinemaMap)
admin.site.register(NameRentalMap)
admin.site.register(NameStreamingServiceMap)
