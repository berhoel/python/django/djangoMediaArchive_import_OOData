"""Application definition."""

from django.apps import AppConfig


class DjangomediaarchiveImportOodataConfig(AppConfig):
    """Application class."""

    default_auto_field = "django.db.models.AutoField"
    name = "berhoel.django.media_ooimport"
    verbose_name = "LibreOffice importer for DjangoMedia"
