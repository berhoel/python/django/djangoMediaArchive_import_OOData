#! /usr/bin/env python
"""General definitions for testing djangoMediaArchive_media_OOImport."""

import pytest

__date__ = "2024/08/09 19:10:50 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2022 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    db
    pass
