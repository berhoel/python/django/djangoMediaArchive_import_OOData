"""Django models for series application."""

from typing import ClassVar
import uuid

from django.db import models

from berhoel.django.media.models import DVD, Media, Person  # isort:skip

__date__ = "2024/08/09 19:58:17 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# Create your models here.


class SaveLine(models.Model):
    """Store original line in 'k&v' for DVD."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    dvd = models.OneToOneField(DVD, on_delete=models.CASCADE)
    line = models.IntegerField()

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["line"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.line}: {self.dvd.name}"


class ConvertedSheet(models.Model):
    """Which sheets are already saved?."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    saved = models.CharField(max_length=255)

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.saved}"


class NameMediaMap(models.Model):
    """Map for non DVD Media."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    media = models.ForeignKey(
        Media,
        on_delete=models.CASCADE,
        related_name="name_media_maps",
        related_query_name="name_media_map",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["name"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}: {self.media.name}"


class NameCinemaMap(models.Model):
    """Map for cinemas."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="name_cinema_maps",
        related_query_name="name_cinema_map",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["name"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}: {self.person.name}"


class NameRentalMap(models.Model):
    """Map for Rental companies."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="name_rental_maps",
        related_query_name="name_rental_map",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["name"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}: {self.person.name}"


class NameStreamingServiceMap(models.Model):
    """Map for streaming services."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255)
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        related_name="name_streaming_service_maps",
        related_query_name="name_streaming_service_map",
    )

    class Meta:
        """Configure Django model class."""

        ordering: ClassVar = ["name"]

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.name}: {self.person.name}"
