..
  Task:

  :Authors:
    - `Berthold Höllmann <berhoel@gmail.com>`__
  :Organization: Berthold Höllmann
  :Time-stamp: <2023-01-08 17:39:09 hoel>
  :datestamp: %Y-%m-%d
  :Copyright: Copyright © 2020 by Berthold Höllmann

.. include:: ../README.rst

API Documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:


   berhoel.django
   berhoel.django.media_ooimport
   berhoel.django.media_ooimport.admin
   berhoel.django.media_ooimport.apps
   berhoel.django.media_ooimport.models
   berhoel.django.media_ooimport.tests
   berhoel.django.media_ooimport.views
   berhoel.django.media_ooimport.console
   berhoel.django.media_ooimport.console._helper
   berhoel.django.media_ooimport.console.filme
   berhoel.django.media_ooimport.console.kuv
   berhoel.django.media_ooimport.console.serie
   berhoel.django.media_ooimport.console.tests.test_helper
   berhoel.django.media_ooimport.settings
   berhoel.django.media_ooimport.migrations
   berhoel.django.media_ooimport.migrations.0001_initial
   berhoel.django.media_ooimport.migrations.0002_alter_namecinemamap_options_and_more

.. toctree::
   :caption: Contents:
   :name: mastertoc
   :maxdepth: 1


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
