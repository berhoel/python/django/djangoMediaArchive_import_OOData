[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

# djangoMediaArchive_import_OOData #

Addon for `djangoMediaArchive
https://github.com/berhoel/djangoMediaArchive. Import data for
djangoMediaArchive from LibreOffice spreadsheet.

## Quick start ##

1. Add "djangoMediaArchive_import_OOData" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'berhoel.django.media_ooimport',
    ]

2. Include the djangoMediaArchive_import_OOData URLconf in your project urls.py like this::

    path('media_ooimport/', include('berhoel.django.media_ooimport.urls')),

3. Run `python manage.py migrate` to create the djangoMediaArchive_import_OOData models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a inspect media data (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/media_ooimport/ to import the media information.
